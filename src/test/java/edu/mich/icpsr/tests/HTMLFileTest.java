package edu.mich.icpsr.tests;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Ignore;
import org.junit.Test;

public class HTMLFileTest {

	@Ignore
	@Test
	public void testFile() throws IOException{
		File f = new File("src/test/resources/encodingIssue.html");
		 byte[] encoded = Files.readAllBytes(Paths.get("src/test/resources/encodingIssue.html"));
		  String s =  new String(encoded, "UTF-8");
		  System.out.println(s);
	}
}
