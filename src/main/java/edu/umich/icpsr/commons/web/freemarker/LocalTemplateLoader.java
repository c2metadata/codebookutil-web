package edu.umich.icpsr.commons.web.freemarker;

import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.jsp.jstl.core.Config;

import org.apache.log4j.Logger;

import freemarker.cache.URLTemplateLoader;

public class LocalTemplateLoader extends URLTemplateLoader {

	private static final Logger LOG = Logger.getLogger(LocalTemplateLoader.class);

	@Override
	protected URL getURL(String paramString) {
		LOG.debug("Executing getURL for paramString = " + paramString + ".");
		return getFreemarkerURL(paramString);
	}
	
	public URL getFreemarkerURL(String paramString){
		LOG.debug("Executing getFreemarkerURL for paramString = " + paramString + ".");		
		String urlString =  "http://52.90.103.106:8091/codebookutil-web/resources/templates/htmlCodebook_en_US.ftlh";
		
		LOG.debug("urlString = " + urlString);
		try {
			return new URL(urlString);
		} catch (MalformedURLException e) {
			LOG.error(e.getMessage(), e);
		}
		return null;
	}
	
}
