package edu.umich.icpsr.c2metadata.service;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import edu.umich.icpsr.c2metadata.util.CodebookBuilder;
import edu.umich.icpsr.c2metadata.util.DDIUtils;
import edu.umich.icpsr.ddi2.schema.CodeBookType;

@Component
@PropertySource(value = "${config.loc:classpath:config.properties}", ignoreResourceNotFound = false)
public class CodeBookService {

	private static final Logger LOG = Logger.getLogger(CodeBookService.class);

	@Autowired
	@Qualifier("codebookBuilder")
	private CodebookBuilder codebookBuilder;

	public CodeBookService() {

	}

	public String generateCodebook(String tempDir, String ddiPath, boolean path) {
		LOG.info("Executing generateCodebook.");
		String codebookPath = null;
		try {
			File ddiFile = loadFileFromFilePath(ddiPath);
			CodeBookType codebook = DDIUtils.loadFromXml(ddiFile);
			String html = codebookBuilder.buildCodebook(codebook);
			if (html == null || html.isEmpty()) {

				// sendCodebookError(job, null);
				return null;
			} else {
				codebookPath = tempDir + UUID.randomUUID().toString() + "/codebook.html";
				File codebookFile = new File(codebookPath);
				FileUtils.writeStringToFile(codebookFile, html);
			}
			if (path) {
				return codebookPath;
			}
			return html;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return null;
	}

	private File loadFileFromFilePath(String filePath) {
		// LOG.info("Executing loadFileFromFilePath for filePath = " +
		// filePath);
		try {
			Path path = Paths.get(filePath);
			File file = new File(path.toString());
			return file;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return null;
	}

}