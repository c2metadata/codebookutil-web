package edu.umich.icpsr.c2metadata.util;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import edu.umich.icpsr.ddi2.schema.CodeBookType;
import freemarker.template.Configuration;
import freemarker.template.Template;

@Component
@PropertySource(value = "${config.loc:classpath:config.properties}", ignoreResourceNotFound = false)
public class CodebookBuilder {

	private static final Logger LOG = Logger.getLogger(CodebookBuilder.class);

	@Autowired
	private Environment env;
	@Autowired
	@Qualifier("freemarkerConfigLocal")
	private FreeMarkerConfigurer freemarkerConfigLocal;

	@Autowired
	@Qualifier("freemarkerConfig")
	private FreeMarkerConfigurer freemarkerConfig;

	public String buildCodebookLocally(CodeBookType codebook) {
		LOG.info("Executing buildCodebookLocally.");
		Map<String, Object> pageMap = new HashMap<String, Object>();
		pageMap.put("codebook", codebook);
		Configuration cfg = freemarkerConfigLocal.getConfiguration();
		Template template = null;
		try {
			cfg.clearTemplateCache();
			template = cfg.getTemplate("/codebookutil-web/src/main/webapp/resources/templates/applications/c2m/default/freemarker/htmlCodebook");
			StringWriter out = new StringWriter();
			template.process(pageMap, out);
			return out.toString();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return null;
	}
	public String buildCodebook(CodeBookType codebook) {
		LOG.info("Executing buildCodebook.");
//		if(config.getValue("build.environment").equals("local")) {
//			return buildCodebookLocally(codebook);
//		}
		try {
			Map<String, Object> pageMap = new HashMap<String, Object>();
			pageMap.put("codebook", codebook);
			Template template = null;
			Configuration cfg = freemarkerConfig.getConfiguration();
			cfg.setClassForTemplateLoading(this.getClass(), "/");

			template = cfg.getTemplate("htmlCodebook_en_US.ftlh");
			StringWriter out = new StringWriter();
			template.process(pageMap, out);
			String output = out.toString();
			out.close();
			return output;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} 
		return null;
	}

	
}
