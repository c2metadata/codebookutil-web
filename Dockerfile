FROM tomcat:9.0.19-jre8-alpine

LABEL maintainer="C2Metadata"

COPY ./target/codebookutil-web.war /tmp
#COPY codebookutil-web.war /tmp

RUN mkdir /usr/local/tomcat/webapps/codebookutil-web \
    && unzip /tmp/codebookutil-web.war -d /usr/local/tomcat/webapps/codebookutil-web
