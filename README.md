## Docker Image
Run the docker image with the following command (replace `:latest` with the version you want to use, like `:SDTL0.3`)
`docker run -d -p 8080:8080 registry.gitlab.com/c2metadata/codebookutil-web:latest`